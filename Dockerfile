# syntax = docker/dockerfile:1.4
FROM alpine:3.17.3
ARG GLOW_VER=1.5.1
ARG TARGETPLATFORM
WORKDIR /opt/glow
RUN apk add bash
RUN <<eot bash
if [[ "$TARGETPLATFORM" == "linux/amd64" ]];then
  wget -qO - https://github.com/charmbracelet/glow/releases/download/v${GLOW_VER}/glow_Linux_x86_64.tar.gz |tar xfz -
else
  wget -qO - https://github.com/charmbracelet/glow/releases/download/v${GLOW_VER}/glow_Linux_arm64.tar.gz |tar xfz - 
fi
eot

